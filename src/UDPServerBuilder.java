import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

public class UDPServerBuilder extends UDPInfo {

	InetSocketAddress isA; // the remote address ( LOCAL HOST / PORT )
	DatagramSocket s; // the socket object ( SOCKET )
	DatagramPacket req, rep; // to prepare the request and reply messages ( PACKET ) // REQUEST TO SEND &
								// RESPONSE TO RECIEVE
	final int size = 2048; // the default size for the buffer array

	UDPServerBuilder() {
		isA = new InetSocketAddress("localhost", 8080);
		s = null;
		req = rep = null;
	}

	protected void setConnection() throws IOException {
		s = new DatagramSocket();

		isA = new InetSocketAddress("localhost", 8080);

		/** we can include more setting, later � */
	}
}
